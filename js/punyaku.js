// Ajax for tambah user
var tambahUser = document.querySelector("#tambahUser");
	
	tambahUser.addEventListener("click", function() {
		var userBaru = { 
			"username" : document.querySelector('.tambah-user-baru [name="username"]').value,
			"email" : document.querySelector('.tambah-user-baru [name="email"]').value,
			"first_name" : document.querySelector('.tambah-user-baru [name="first_name"]').value,
			"last_name" : document.querySelector('.tambah-user-baru [name="last_name"]').value,
			"url" : document.querySelector('.tambah-user-baru [name="url"]').value,
			"password" : document.querySelector('.tambah-user-baru [name="password"]').value,
			"name" : " ",
			"description" : " ",
			"locale": "en_US",
			"nickname":" ",
			"slug":" ",
			"roles": "contributor",
			"metas": "view"
		}

		var createPost = new XMLHttpRequest();
		createPost.open("POST", "https://barubelajarku.000webhostapp.com/wp-json/wp/v2/users");
		createPost.setRequestHeader("X-WP-Nonce", dataAuth.nonce);
		createPost.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		createPost.send(JSON.stringify(userBaru));
	});